package service.numeral.management

import spock.lang.Subject
import spock.lang.Title

/**
 * Created by Golden
 */
@Title("Unit Tests For ManageIntergalacticUnits")
@Subject(ManageIntergalacticUnits)
class ManageIntergalacticUnitsSpec extends spock.lang.Specification{

    ManageIntergalacticUnits manageIntergalacticUnits = new ManageIntergalacticUnits()

    def'Should Convert Intergalactic Units To Roman Numerals'(){
        given:'Intergalactic Units'
        String intergalacticUnits = "glob glob prok"

        manageIntergalacticUnits.addIntergalacticUnits("glob", (char)'I')
        manageIntergalacticUnits.addIntergalacticUnits("prok", (char)'V')

        when:'convertIntergalacticUnitsToRomanNumerals Is Called'
        String response = manageIntergalacticUnits.convertIntergalacticUnitsToRomanNumerals(intergalacticUnits.split(" ").toList())

        then:'A RomanNumeral Equivalent Should Be Returned'
        response == "IIV"


    }

}
