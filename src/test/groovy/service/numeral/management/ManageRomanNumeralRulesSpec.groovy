package service.numeral.management

import spock.lang.Subject
import spock.lang.Title
import spock.lang.Unroll

/**
 * Created by Golden.
 *
 * The symbols "I", "X", "C", and "M" can be repeated three times in succession, but no more.
 * "D", "L", and "V" can never be repeated
 */
@Title("Unit Tests For ManageRomanNumeralRules")
@Subject(ManageRomanNumeralRules)
class ManageRomanNumeralRulesSpec extends spock.lang.Specification{

    ManageRomanNumeralRules manageRomanNumeralRules = new ManageRomanNumeralRules()
    boolean isSymbolValid
    String romanNumerals
    char firstRomanSymbol
    char secondRomanSymbol

    @Unroll('RepeatedSymbols #repeatedSymbols | ExpectedResult #expectedResult')
    def'Should Not Repeat Roman Numeral Symbols In Succession Beyond Allowed Repetitions.'() {
        given: 'A String Roman Numeral With Unaccepted Symbol Repetitions'
        romanNumerals = repeatedSymbols

        when: 'isSymbolRepeatedCorrectly is called'
        isSymbolValid = manageRomanNumeralRules.isSymbolRepeatedCorrectly(romanNumerals)

        then: 'We Should Get Boolean False As Response For All Violations'
        isSymbolValid == expectedResult

        where: 'The Repeated Roman Numerals, In Succession, Beyond Allowed Repetitions Are'
        repeatedSymbols| expectedResult
        "IIII" | false
        "XXXX" | false
        "CCCC" | false
        "MMMM" | false
        "DD" | false
        "LL" | false
        "VV" | false
        "XXXIX" | true
        "XXXVCCCC" | false
    }

    @Unroll('someNextRomanNumeral #someNextRomanNumeral, expectedResult #expectedResult')
    def'Should Only Subtract I From V and X only'(){
        given:'A Roman Numeral Containing "I"'
        firstRomanSymbol = 'I'
        secondRomanSymbol = someNextRomanNumeral

        when:'isSubtractionRuleViolated Is Called'
        isSymbolValid = manageRomanNumeralRules.isSubtractionRuleViolated(firstRomanSymbol ,secondRomanSymbol)

        then: 'We Should Get Boolean False As Response For All Violations'
        isSymbolValid == expectedResult

        where: 'The Various Roman Numerals Containing I Are'
        someNextRomanNumeral | expectedResult
        'X' | false
        'V' | false
    }

    @Unroll('someNextRomanNumeral #someNextRomanNumeral, exception #exception')
    def'Should Throw A SymbolSubtractionRuntimeException IF "I" Is Subtract From Other Symbols '(){
        given:'A Roman Numeral Containing "I"'
        firstRomanSymbol = 'I'
        secondRomanSymbol = someNextRomanNumeral

        when:'isSubtractionRuleViolated Is Called'
        isSymbolValid = manageRomanNumeralRules.isSubtractionRuleViolated(firstRomanSymbol ,secondRomanSymbol)

        then: 'A SymbolSubtractionRuntimeException Should Be Raised'
        thrown(exception)

        where: 'The Various Roman Numerals Containing I Are'
        someNextRomanNumeral | exception
        'M' |  SymbolSubtractionRuntimeException
        'C' | SymbolSubtractionRuntimeException
    }

    @Unroll('someNextRomanNumeral #someNextRomanNumeral, expectedResult #expectedResult')
    def'Should Only Subtract X From L and C only'(){
        given:'A Roman Numeral Containing "X"'
        firstRomanSymbol = 'X'
        secondRomanSymbol = someNextRomanNumeral

        when:'isSubtractionRuleViolated Is Called'
        isSymbolValid = manageRomanNumeralRules.isSubtractionRuleViolated(firstRomanSymbol ,secondRomanSymbol)

        then: 'We Should Get Boolean False As Response For All Violations'
        isSymbolValid == expectedResult

        where: 'The Various Roman Numerals Containing X Are'
        someNextRomanNumeral | expectedResult
        'L' | false
        'C' | false
    }

    @Unroll('someNextRomanNumeral #someNextRomanNumeral, exception #exception')
    def'Should Throw A SymbolSubtractionRuntimeException IF "X" Is Subtract From Other Symbols'(){
        given:'A Roman Numeral Containing "X"'
        firstRomanSymbol = 'X'
        secondRomanSymbol = someNextRomanNumeral

        when:'isSubtractionRuleViolated Is Called'
        isSymbolValid = manageRomanNumeralRules.isSubtractionRuleViolated(firstRomanSymbol ,secondRomanSymbol)

        then: 'A SymbolSubtractionRuntimeException Should Be Raised'
        thrown(exception)

        where: 'The Various Roman Numerals Containing X Are'
        someNextRomanNumeral | exception
        'D' | SymbolSubtractionRuntimeException
        'M' | SymbolSubtractionRuntimeException
    }

    def'Should Only Subtract C From D and M only'(){
        given:'A Roman Numeral Containing "C"'
        firstRomanSymbol = 'C'
        secondRomanSymbol = someNextRomanNumeral

        when:'isSubtractionRuleViolated Is Called'
        isSymbolValid = manageRomanNumeralRules.isSubtractionRuleViolated(firstRomanSymbol ,secondRomanSymbol)

        then: 'We Should Get Boolean False As Response For All Violations'
        isSymbolValid == expectedResult

        where: 'The Various Roman Numerals Containing C Are'
        someNextRomanNumeral | expectedResult
        'D' | false
        'M' | false
    }

    @Unroll('someNextRomanNumeral #someNextRomanNumeral, exception #exception')
    def'Should Throw A SymbolSubtractionRuntimeException IF "C" Is Subtract From Other Symbols'(){
        given:'A Roman Numeral Containing "C"'
        firstRomanSymbol = 'C'
        secondRomanSymbol = 'Z'

        when:'isSubtractionRuleViolated Is Called'
        isSymbolValid = manageRomanNumeralRules.isSubtractionRuleViolated(firstRomanSymbol ,secondRomanSymbol)

        then: 'A SymbolSubtractionRuntimeException Should Be Raised'
        thrown(exception)

        where: 'The Various Roman Numerals Containing X Are'
        someNextRomanNumeral | exception
        'Z' | SymbolSubtractionRuntimeException
    }

    @Unroll('someNextRomanNumeral #someNextRomanNumeral, exception #exception')
    def'Should Throw A SymbolSubtractionRuntimeException IF "V","L","D" Is Subtract From Other Symbols'(){
        given:'A Roman Numeral Containing "C"'
        firstRomanSymbol = someFirstRomanNumeral
        secondRomanSymbol = someNextRomanNumeral

        when:'isSubtractionRuleViolated Is Called'
        isSymbolValid = manageRomanNumeralRules.isSubtractionRuleViolated(firstRomanSymbol ,secondRomanSymbol)

        then: 'A SymbolSubtractionRuntimeException Should Be Raised'
        thrown(exception)

        where: 'The Various Roman Numerals Containing C Are'
        someFirstRomanNumeral | someNextRomanNumeral | exception
        'V' | 'X' | SymbolSubtractionRuntimeException
        'L' | 'C' | SymbolSubtractionRuntimeException
        'D' | 'Z' | SymbolSubtractionRuntimeException
    }
}
