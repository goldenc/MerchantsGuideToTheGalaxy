package service.numeral.management;

/**
 * Created by Golden
 */
public class RepetitionViolationException extends RuntimeException {

    public RepetitionViolationException(String message) {
        super(message);
    }
}
