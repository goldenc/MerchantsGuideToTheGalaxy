package service.numeral.management;

import domain.RomanNumeral;

/**
 * Created by golden.
 */
public class ManageRomanNumeral {

    ManageRomanNumeralRules manageRomanNumeralRules;

    public ManageRomanNumeral(){
        this.manageRomanNumeralRules = new ManageRomanNumeralRules();
    }

    public int convertRomanNumeralToNumber(String romanNumeral){

        checkSymbolRepetitionViolation(romanNumeral);
        int result = 0;

        if(romanNumeral.length() == 1){
            result =  RomanNumeral.valueOf(romanNumeral).getValue();
        }else if (romanNumeral.length() > 1){

            char[] romanNumeralSymbols= romanNumeral.toCharArray();

            for(int i =0 ; i < romanNumeralSymbols.length; i++){

                Character currentSymbol = romanNumeralSymbols[i];
                int currentSymbolValue = RomanNumeral.valueOf(currentSymbol.toString()).getValue();

                if(i==romanNumeralSymbols.length-1){
                  return result += currentSymbolValue;
                }

                Character nextSymbol = romanNumeralSymbols[i+1];
                int nextSymbolValue = RomanNumeral.valueOf(nextSymbol.toString()).getValue();
                if(currentSymbolValue < nextSymbolValue && !manageRomanNumeralRules.isSubtractionRuleViolated(currentSymbol,nextSymbol)){
                    result -= currentSymbolValue;
                }
                if(currentSymbolValue >= nextSymbolValue){
                    result += currentSymbolValue;
                }
            }
        }
        return result;
    }

    private void checkSymbolRepetitionViolation(String romanNumeral) {
        if(!manageRomanNumeralRules.isSymbolRepeatedCorrectly(romanNumeral)){
            throw new RepetitionViolationException("Too Many Succession Repetitions On Roman Symbols " + romanNumeral);
        }
    }
}
