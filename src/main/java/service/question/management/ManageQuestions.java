package service.question.management;

import service.numeral.management.*;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Golden
 */
public class ManageQuestions {

    private ManageIntergalacticUnits manageIntergalacticUnits;
    private ManageRomanNumeral manageRomanNumeral;
    private ManageMetalCredits manageMetalCredits;
    ManageQuestionsHelper manageQuestionsHelper;
    String response = unKnownRequestResponse;
    public static final String unKnownRequestResponse = "I have no idea what you are talking about";

    public ManageQuestions(ManageMetalCredits manageMetalCredits, ManageIntergalacticUnits manageIntergalacticUnits){
        this.manageIntergalacticUnits = manageIntergalacticUnits;
        this.manageRomanNumeral = new ManageRomanNumeral();
        this.manageMetalCredits = manageMetalCredits;
    }

    public String computeTheHowMuchQuestion(String question){

        manageQuestionsHelper = new ManageQuestionsHelper(question).invoke(12);
        List<String> wordsFromQuestion = manageQuestionsHelper.getWordsFromQuestion();
        question = manageQuestionsHelper.getQuestion();

        try {

            String romanNumeralValue = manageIntergalacticUnits.convertIntergalacticUnitsToRomanNumerals(wordsFromQuestion);
            int numberValue = manageRomanNumeral.convertRomanNumeralToNumber(romanNumeralValue);
            response = question + " is " + numberValue;

        }catch (SymbolNotFoundRunException e){
            return unKnownRequestResponse;
        }catch (SymbolSubtractionRuntimeException e){
            return unKnownRequestResponse;
        }catch (RepetitionViolationException e){
            return unKnownRequestResponse;
        }
        return response;
    }

    public String computeTheHowManyQuestion(String question){

        manageQuestionsHelper = new ManageQuestionsHelper(question).invoke(20);
        List<String> wordsFromQuestion = manageQuestionsHelper.getWordsFromQuestion();
        question = manageQuestionsHelper.getQuestion();
        String metal = wordsFromQuestion.get(wordsFromQuestion.size() - 1);

        if(!manageMetalCredits.getIntergalacticToRomanMap().containsKey(metal)){
            return  unKnownRequestResponse;
        }

        try {

            String romanNumeralValue = manageIntergalacticUnits.convertIntergalacticUnitsToRomanNumerals(
                    wordsFromQuestion.subList(0, wordsFromQuestion.size() - 1));

            int numberValue = manageRomanNumeral.convertRomanNumeralToNumber(romanNumeralValue);
            double credit = manageMetalCredits.getIntergalacticToRomanMap().get(metal);
            double credits = credit*numberValue;
            NumberFormat numberFormat = NumberFormat.getInstance();
            numberFormat.setParseIntegerOnly(true);
            response = question + " is " + numberFormat.parse(String.valueOf(credits)) + " Credits";
        }catch (SymbolNotFoundRunException e){
            return unKnownRequestResponse;
        }catch (SymbolSubtractionRuntimeException e){
            return unKnownRequestResponse;
        }catch (RepetitionViolationException e){
            return unKnownRequestResponse;
        }catch (ParseException pe) {
            System.out.println("Technical Error ");
        }
        return response;
    }

    private class ManageQuestionsHelper {
        private String question;
        private List<String> wordsFromQuestion;

        public ManageQuestionsHelper(String question) {
            this.question = question;
        }

        public String getQuestion(){
            return  this.question;
        }


        public List<String> getWordsFromQuestion() {
            return wordsFromQuestion;
        }

        public ManageQuestionsHelper invoke(int beginIndex) {
            question = question.subSequence(beginIndex,question.length()-2).toString();
            wordsFromQuestion = Arrays.asList(question.split(" "));
            return this;
        }
    }
}
