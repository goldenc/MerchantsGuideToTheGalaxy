package inputOut;

import domain.InputSequenceType;
import service.numeral.management.ManageIntergalacticUnits;
import service.numeral.management.ManageMetalCredits;
import service.numeral.management.ManageRomanNumeral;
import service.question.management.ManageQuestions;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Golden
 */
public class InputProcessor {

    private ManageIntergalacticUnits manageIntergalacticUnits;
    private ManageMetalCredits manageMetalCredits;
    private ManageQuestions manageQuestions;

    public InputProcessor(){
        this.manageIntergalacticUnits = new ManageIntergalacticUnits();
        this.manageMetalCredits = new ManageMetalCredits(manageIntergalacticUnits);
        this.manageQuestions = new ManageQuestions(manageMetalCredits, manageIntergalacticUnits);
    }

    public void processLines(List<String> inputLines){
        String answer = "";
        for(String inputLine: inputLines){
            answer = processInput(inputLine, answer);
            if(!answer.isEmpty()){
                System.out.println(answer);
            }
        }
    }

    public String processInput(String inputLine, String answer){
        getInstanceTypes();
        answer = "";
        Pattern inputSequence;
        Matcher matcher;

        for(InputSequenceType inputSequenceType: InputSequenceType.values()){

            inputSequence = Pattern.compile(inputSequenceType.getRegex());
            matcher = inputSequence.matcher(inputLine);

            if(matcher.find()){
                switch (inputSequenceType){
                    case ASSIGNMENT_EXPRESSION:
                        saveNewUnits(inputLine);
                        break;
                    case METAL_CREDIT_DATA:
                        saveMetalCreditData(matcher);
                        break;
                    case HOW_MUCH_QUESTION_SYNTAX:
                        answer = manageQuestions.computeTheHowMuchQuestion(inputLine);
                        break;
                    case HOW_MANY_QUESTION_SYNTAX:
                        answer = manageQuestions.computeTheHowManyQuestion(inputLine);
                }
            }
        }

        if(answer.isEmpty() && inputLine.contains("how much")){
            return  ManageQuestions.unKnownRequestResponse;
        }

        return answer;
    }

    private void getInstanceTypes() {
        InputSequenceType ASSIGNMENT_EXPRESSION = InputSequenceType.ASSIGNMENT_EXPRESSION;
        InputSequenceType METAL_CREDIT_DATA = InputSequenceType.METAL_CREDIT_DATA;
        InputSequenceType HOW_MUCH_QUESTION_SYNTAX = InputSequenceType.HOW_MUCH_QUESTION_SYNTAX;
        InputSequenceType HOW_MANY_QUESTION_SYNTAX = InputSequenceType.HOW_MANY_QUESTION_SYNTAX;
    }

    private void saveMetalCreditData(Matcher matcher) {
        int credits = Integer.valueOf(matcher.group(3));
        List<String> intergalacticUnits = Arrays.asList(matcher.group(1).split(" "));
        double singleMetalCredit = manageMetalCredits.computeSingleMetalCreditValue(intergalacticUnits,credits);

        manageMetalCredits.addMetalToMap(matcher.group(2), singleMetalCredit);
    }

    private void saveNewUnits(String inputLine) {
        String [] keyValue = inputLine.split(" ");
        if(!manageIntergalacticUnits.getIntergalacticToRomanNumeralsMap().containsKey(keyValue[0])){
            manageIntergalacticUnits.addIntergalacticUnits(keyValue[0], keyValue[2].charAt(0));
        }
    }
}