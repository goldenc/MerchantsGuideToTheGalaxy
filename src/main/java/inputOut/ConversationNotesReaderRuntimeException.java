package inputOut;

/**
 * Created by Golden
 */
public class ConversationNotesReaderRuntimeException extends RuntimeException {

    public ConversationNotesReaderRuntimeException(String message, Exception exception) {
        super(message, exception);
    }
}
