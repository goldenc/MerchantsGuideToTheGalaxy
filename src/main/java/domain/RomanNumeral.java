package domain;

/**
 * Created by Golden.
 *
 * The symbols "I", "X", "C", and "M" can be repeated three times in succession, but no more.
 * "D", "L", and "V" can never be repeated
 */
public enum RomanNumeral {

    I(1, "IIII+"),
    V (5, "VV+"),
    X (10, "XXXX+"),
    L (50,"LL+"),
    C (100, "CCCC+"),
    D (500, "DD+"),
    M (1000, "MMMM+");

    private int value;
    private  String repetitionsNotAllowed;

    private RomanNumeral(int value, String repetitionsNotAllowed){
        this.value = value;
        this.repetitionsNotAllowed = repetitionsNotAllowed;
    }

    public int getValue(){
        return value;
    }

    public String getRepetitionsNotAllowed() {
        return repetitionsNotAllowed;
    }
}
