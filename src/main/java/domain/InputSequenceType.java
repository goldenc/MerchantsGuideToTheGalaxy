package domain;

/**
 * Created by Golden
 */
public enum InputSequenceType {

    ASSIGNMENT_EXPRESSION("^([a-zA-Z]+) is ([I|V|X|L|C|D|M])$"),
    METAL_CREDIT_DATA("((?:[a-z]+ )+)([A-Z]\\w+) is (\\d+) ([A-Z]\\w+)$"),
    HOW_MUCH_QUESTION_SYNTAX("how much is ((?:\\w+ )+)\\?$"),
    HOW_MANY_QUESTION_SYNTAX("how many ([a-zA-Z]\\w+) is ((?:\\w+ )+)([A-Z]\\w+) \\?$");

    private  String regex;

    private InputSequenceType(String regex){
        this.regex = regex;
    }

    public String getRegex() {
        return regex;
    }
}
