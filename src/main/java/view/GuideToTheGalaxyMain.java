package view;

import inputOut.ConversationNotesReader;
import inputOut.InputProcessor;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

/**
 * Created by Golden
 */
public class GuideToTheGalaxyMain {

    public  static void main(String [] args){

        System.out.println("Merchant's Guide To The Galaxy App running: ");
        Path filePath = Paths.get(args[0]);

        ConversationNotesReader conversationNotesReader = new ConversationNotesReader(filePath);
        List<String > conversationNotes =conversationNotesReader.readNotesFromFile();

        System.out.println("Output Results: "+ "\n");
        new InputProcessor().processLines(conversationNotes);
    }
}
